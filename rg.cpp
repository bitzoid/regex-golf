#include <regex>
#include <string>
#include <vector>
#include <iostream>
#include <array>
#include <cctype>
#include <thread>
#include <chrono>
#include <initializer_list>
#include <tuple>

// https://xkcd.com/1313

struct Query {
  std::vector<std::string> positive;
  std::vector<std::string> negative;
  std::string alphabet;

  std::vector<std::string> positiveL;
  std::vector<std::string> negativeL;

  Query(std::vector<std::string> positive, std::vector<std::string> negative)
    : positive{std::move(positive)}, negative{std::move(negative)}, alphabet{extractAlphabet(this->positive, this->negative)} {
      positiveL = this->positive;
      for (auto& p: positiveL) {
        std::transform(std::begin(p), std::end(p), std::begin(p), [](char const c) -> char {
          return static_cast<char>(std::tolower(static_cast<unsigned char>(c)));
        });
      }
      negativeL = this->negative;
      for (auto& n: negativeL) {
        std::transform(std::begin(n), std::end(n), std::begin(n), [](char const c) -> char {
          return static_cast<char>(std::tolower(static_cast<unsigned char>(c)));
        });
      }
      for (auto const& n: this->negativeL) {
        if (std::find(std::begin(this->positiveL), std::end(this->positiveL), n) != std::end(this->positiveL)) {
          throw std::invalid_argument("Word '" + n + "' present in both positive and negative list.");
        }
      }
  }

  static constexpr std::string specialChars() {
    return "[-]|+*?.^$";
  }
  static constexpr std::string qualChars() {
    return "?*";
  }
  static constexpr std::string noStartChars() {
    return "-]|+*?$";
  }

  static std::string extractAlphabet(std::vector<std::string> const& pos, std::vector<std::string> const& neg) {
    std::array<std::uint8_t, 256> table{0};
    auto const addStr = [&table](std::string const& s) {
      for (auto const c: s) {
        table[static_cast<std::uint8_t>(std::tolower(static_cast<int>(c)))] = 1;
      }
    };
    std::for_each(begin(pos),end(pos),addStr);
    std::for_each(begin(neg),end(neg),addStr);
    std::array<std::string,2> chars;
    for (std::size_t i = 0; i < 256; ++i) {
      if (table[i]) {
        auto const c = static_cast<char>(i);
        chars[(('a' <= c) & (c <= 'z')) | (c == ' ')].push_back(c);
      }
    }
    if (!chars[0].empty()) {
      throw std::invalid_argument("Unsupported characters in input query: " + chars[0]);
    }
    return chars[1] + specialChars();
  }
};

struct Checker {

  bool operator()(std::regex const& r, Query const& query) const {
    for (auto const& p: query.positive) {
      if (!std::regex_search(p, r)) {
        return false;
        //std::cerr << "E: matches-not-but-should: " << p << '\n';
      }
    }
    for (auto const& n: query.negative) {
      if (std::regex_search(n, r)) {
        //std::cerr << "E: match-but-shouldn't: " << n << '\n';
        return false;
      }
    }
    return true;
  }

  bool operator()(std::string const& r, Query const& query) const {
    try {
      return (*this)(std::regex(r,std::regex_constants::icase), query);
    } catch (std::regex_error const&) {
      std::cerr << "weird re: '" << r << "'\n";
      return false;
    }
  }
};


template <std::uint8_t N>
struct TableAutomaton {
  static constexpr std::size_t TSz = 128;
  std::array<std::array<std::uint8_t, TSz>, N+1> jumpTable;
  std::uint8_t endStateMax;

  constexpr TableAutomaton(std::uint8_t const endStateMax = N) : jumpTable{}, endStateMax{std::min(N,endStateMax)} {
    for (std::uint8_t i = 0; i < jumpTable.size(); ++i) {
      std::fill(std::begin(jumpTable[i]), std::end(jumpTable[i]), i);
    }
  }

  constexpr TableAutomaton(std::array<std::array<std::uint8_t, TSz>, N> jt, std::uint8_t const endStateMax = N)
    : TableAutomaton(endStateMax) {
    for (std::size_t from = 0; from < N; ++from) {
      for (std::size_t via = 0; via < TSz; ++via) {
        jumpTable[from][via] = std::min(jt[from][via], N);
      }
    }
  }

  TableAutomaton(std::initializer_list<std::tuple<std::uint8_t,char,std::uint8_t>> il, std::uint8_t const endStateMax = N)
    : TableAutomaton(endStateMax) {
    for (auto [from,via,to]: il) {
      if (from >= N) {
        throw std::logic_error("Requested jump from state " + std::to_string(from) + " in automation with states [0..." + std::to_string(N-1) + "]");
      }
      if (via) {
        jumpTable[from][via] = std::min(N, to);
      } else {
        std::fill(std::begin(jumpTable[from]), std::end(jumpTable[from]), std::min(N, to));
      }
    }
  }
  template <typename F>
  requires (std::is_nothrow_invocable_r_v<void,F,std::uint8_t,char,std::uint8_t&>)
  TableAutomaton(F const& f, std::uint8_t endStateMax = N)
    : TableAutomaton(endStateMax) {
    for (std::uint8_t i = 0; i < N; ++i) {
      for (std::uint8_t j = 1; j < TSz; ++j) {
        f(i,static_cast<char>(j),jumpTable[i][j]);
        jumpTable[i][j] = std::min(N,jumpTable[i][j]);
      }
    }
  }
  std::size_t getPrefixLength(std::string const& input) const {
    std::uint8_t state = 0;
    std::array<std::size_t, 2> part{input.size(),input.size()};
    for (std::size_t i = 0; i < input.size(); ++i) {
      part[state >= N] = i;
      state = jumpTable[state][input[i]];
    }
    return (state > endStateMax)?(part[0]+1):0;
  }


  void getPrefix(std::string& pref, std::string const& input) const {
    if (auto const sz = getPrefixLength(input)) {
      pref.resize(sz);
      std::copy_n(std::begin(input), sz, std::begin(pref));
    }
  }
};

class SortedGenerator {
  private:
    std::string alphabet;
    std::array<char, 256> chain;
    std::array<std::uint8_t, 256> ord;
    std::string start;
  public:
    SortedGenerator(std::string alphabet, std::string start = "") : alphabet{std::move(alphabet)}, chain{0}, ord{0}, start{std::move(start)} {
      char next = this->alphabet.front();
      // 'a' <- 'z' <- 'y' <- ... <- 'b' <- 'a'
      for (auto it = std::rbegin(this->alphabet); it != std::rend(this->alphabet); ++it) {
        chain[*it] = next;
        next = *it;
      }
      chain[0] = this->alphabet.front(); // loop sentinel
      for (std::uint8_t i = 0; i < this->alphabet.size(); ++i) {
        ord[this->alphabet[i]] = i;
      }
      if (this->start.empty()) {
        start = std::string{std::begin(alphabet),std::next(std::begin(alphabet))};
      } else {
        for (char const c: this->start) {
          if ((c==0) | (chain[c]==0)) {
            throw std::invalid_argument("Cannot continue from non-alphabet regex '" + this->start + "'. Offending character is '" + std::string{c} + "'.");
          }
        }
      }
    }

    auto const& startRegex() const {
      return start;
    }

    std::uint64_t ordinal(std::string const& reg) const {
      if (reg.empty()) return 0;
      std::uint64_t const b = alphabet.size();
      std::uint64_t const n = reg.size() - 1;
      std::uint64_t prev = 1;
      for (std::size_t i = 0; i < n; ++i)
        prev *= b;
      prev = b*(prev - 1) / (b-1);
      std::uint64_t curr = 0;
      for (auto const c: reg) {
        curr = b*curr + ord[c];
      }
      return prev + curr;
    }

    struct Iterator {
      std::string buf;
      std::array<char, 256> const& chain;

      void advance(std::string& s) const {
        bool zzz = true; // trailing end-of-alphabet
        for (auto it = std::rbegin(s); zzz & (it != std::rend(s)); ++it) {
          zzz = ((*it = chain[*it])) == chain[0];
        }
        if (zzz) {
          s.push_back(chain[0]);
        }
      }

      Iterator& operator++() {
        advance(buf);
        return *this;
      }

      struct FastSkipCB {
        std::string cache{};
        std::string cache2{};
        static constexpr std::array<bool, 256> mkTable(std::string const& chars) {
          std::array<bool, 256> tb{false};
          for (auto const c: chars) {
            tb[c] = true;
          }
          return tb;
        }
        std::string& badPrefix(Query const& query, std::string const& buf) {
          static auto const badTable = FastSkipCB::mkTable(Query::specialChars());
          static auto const qualTable = FastSkipCB::mkTable(Query::qualChars());
          static auto const noStartTable = FastSkipCB::mkTable(Query::noStartChars());
          auto& pref = cache;
          pref.clear();
          if (noStartTable[buf[0]]) {
            pref.resize(1);
            pref[0] = buf[0];
          }
          if (pref.empty()) {
            std::size_t n = 0;
            char next = 0;
            for (std::size_t i = 0; i < buf.size(); ++i) {
              if (badTable[buf[i]]) {
                next = buf[i];
                n = i;
                break;
              }
            }
            if (qualTable[next] & (n > 0))
              --n;
            if (n > 0) {
              auto& test = cache2;
              test.resize(n);
              std::copy_n(std::begin(buf), n, std::begin(test));
              bool matches_any = false;
              while (!test.empty()) {
                matches_any = false;
                for (auto const& p: query.positiveL) {
                  if (matches_any |= p.contains(test))
                    break;
                }
                if (!matches_any) {
                  pref = test;
                  test.pop_back();
                } else {
                  break;
                }
              }
            }
          }
          if (pref.empty() && buf[0] == '^') {
            std::size_t n = 0;
            char next = 0;
            for (std::size_t i = 1; i < buf.size(); ++i) {
              if (badTable[buf[i]]) {
                next = buf[i];
                n = i-1;
                break;
              }
            }
            if (qualTable[next] & (n > 0))
              --n;
            if (n > 0) {
              auto& test = cache2;
              test.resize(n);
              std::copy_n(std::begin(buf)+1, n, std::begin(test));
              bool matches_any = false;
              while (!test.empty()) {
                matches_any = false;
                for (auto const& p: query.positiveL) {
                  if (matches_any |= p.starts_with(test))
                    break;
                }
                if (!matches_any) {
                  pref.resize(1+test.size());
                  pref[0] = '^';
                  std::copy_n(std::begin(test), test.size(), std::begin(pref)+1);
                  test.pop_back();
                } else {
                  break;
                }
              }
            }
          }
          if (pref.empty()) {
            std::size_t n = 0;
            std::size_t start = 0;
            char next = 0;
            if (buf[0] == '^') ++start;
            for (std::size_t i = start; i < buf.size(); ++i) {
              if ((buf[i] == '$') | (buf[i] == '|')) {
                next = buf[i];
                n = i;
                break;
              }
            }
            std::string_view const infix(buf.begin()+start,buf.begin()+n);
            if (!infix.empty()) {
              bool matches_any = false;
              if ((start == 0) & (next == '|')) {
                for (auto const& neg: query.negativeL) {
                  if (matches_any |= neg.contains(infix))
                    break;
                }
              } else if ((start == 1) & (next == '|')) {
                for (auto const& neg: query.negativeL) {
                  if (matches_any |= neg.starts_with(infix))
                    break;
                }
              } else if ((start == 1) & (next == '$')) {
                for (auto const& neg: query.negativeL) {
                  if (matches_any |= (neg == infix))
                    break;
                }
              } else if ((start == 0) & (next == '$')) {
                for (auto const& neg: query.negativeL) {
                  if (matches_any |= neg.ends_with(infix))
                    break;
                }
              }
              if (matches_any) {
                pref = buf;
                pref.resize(n+1);
              }
            }
          }
          if (pref.empty()) { // check correct placement of [] braces, and -
            static TableAutomaton<6> const bracketCheck({
              // 0: outside of [] group
              {0,'[',4},
              {0,']',9},
              {0,'-',9},
              // 4: inside of [] group, - not legal, [ just opened
              {4,'\0',2},
              {4,'^',5},
              {4,']',9},
              {4,'[',9},
              {4,'|',9},
              {4,'+',9},
              {4,'*',9},
              {4,'?',9},
              {4,'.',9},
              {4,'$',9},
              {4,'-',9},
              // 5: inside of [] group, - not legal, [^ just opened
              {5,'\0',2},
              {5,'^',9},
              {5,']',9},
              {5,'[',9},
              {5,'|',9},
              {5,'+',9},
              {5,'*',9},
              {5,'?',9},
              {5,'.',9},
              {5,'$',9},
              {5,'-',9},
              // 1: inside of [] group, - not legal
              {1,'\0',2},
              {1,'^',1},
              {1,']',0},
              {1,'[',9},
              {1,'|',9},
              {1,'+',9},
              {1,'*',9},
              {1,'?',9},
              {1,'.',9},
              {1,'$',9},
              {1,'-',9},
              // 2: inside of [] group, - legal
              {2,']',0},
              {1,'^',9},
              {2,'[',9},
              {2,'|',9},
              {2,'+',9},
              {2,'*',9},
              {2,'?',9},
              {2,'.',9},
              {2,'$',9},
              {2,'-',3},
              // 3: inside of [] group, - just read
              {3,'\0',1},
              {3,'^',9},
              {3,']',9},
              {3,'[',9},
              {3,'|',9},
              {3,'+',9},
              {3,'*',9},
              {3,'?',9},
              {3,'.',9},
              {3,'$',9},
              {3,'-',9},
            }, 0);
            bracketCheck.getPrefix(pref, buf);
          }
          if (pref.empty()) { // check ^, $, and | placement
            static TableAutomaton<4> const bracketCheck({
              // 0: may read ^, $, or |
              {0,'\0',1},
              {0,'[',0},
              {0,'|',3},
              {0,'$',2},
              // 1: may not read ^, may read $, may read |
              {1,'^',9},
              {1,'[',0},
              {1,'|',3},
              {1,'$',2},
              // 2: $ was just read, may only terminate or read |
              {2,'\0',9},
              {2,'|',3},
              // 3: | was just read, must not read |, $, or terminate
              {3,'\0',1},
              {3,'|',9},
              {3,'$',9},
            }, 2);
            bracketCheck.getPrefix(pref, buf);
          }
          if (pref.empty()) { // quantifier placement and non-trivial sets
            static TableAutomaton<5> const quantifierCheck({
              // 0: default, may read quantifiers
              {0,'?',1},
              {0,'+',1},
              {0,'*',1},
              {0,'|',1},
              {0,'^',1},
              {0,'[',2}, // ']' must to remain in class 0
              // 1: may not read quantifiers, immediatly returns to 0
              {1,'\0',0},
              {1,'?',9},
              {1,'+',9},
              {1,'*',9},
              {1,'|',1},
              {1,'[',2},
              {1,'^',1},
              // 2: inside set (0 characters read)
              {2,'\0',3},
              {2,'?',9},
              {2,'+',9},
              {2,'*',9},
              {2,'|',9},
              {2,'[',9},
              {2,']',9}, // <- reading [] puts us in a fail state
              // 3: inside set (1 character read)
              {3,'\0',4},
              {3,'?',9},
              {3,'+',9},
              {3,'*',9},
              {3,'|',9},
              {3,'[',9},
              {3,']',9}, // <- reading [a] puts us in a fail state
              // 4: inside set (>=2 character read)
              {4,'\0',4},
              {4,'?',9},
              {4,'+',9},
              {4,'*',9},
              {4,'|',9},
              {4,'[',9},
              {4,']',0}, // <- reaing [ab] is fine again
            }, 1);
            quantifierCheck.getPrefix(pref, buf);
          }
          if (pref.empty()) {
            static TableAutomaton<31> const orderedRanges([](std::uint8_t const from, char const via, std::uint8_t& to) noexcept {
              if (via == '[') {
                if (from == 0) to = 1;
                else to = 99;
              } else if (via == ']') {
                to = 0;
              } else if ((from >= 1) & (' ' == via)) {
                std::uint8_t const th = 1;
                if (from > th) to = 99;
                else to = th+1;
              } else if ((from >= 1) & ('a' <= via) & (via <= 'z')) {
                auto const th = static_cast<std::uint8_t>(2 + (via - 'a'));
                // if we are in brackets and read 'c' we need to be at least in state 2+2
                //                                        and to go to state 2+3 so we don't read a-c again
                if (from > th) to = 99;
                else to = th+1;
              } else { // everything else stays at its current state
                to = from;
              }
            },0);
            orderedRanges.getPrefix(pref, buf);
          }
          if (pref.empty()) { // groups separated by | must be of non-ascending length
            std::size_t prev = std::numeric_limits<std::size_t>::max();
            std::size_t curr = 0;
            for (std::size_t i = 0; i < buf.size(); ++i) {
              if (buf[i] == '|') {
                prev = curr;
                curr = 0;
              } else {
                curr++;
              }
              if (curr > prev) {
                pref.resize(i+1);
                std::copy_n(std::begin(buf), i+1, std::begin(pref));
                break;
              }
            }
          }
          if (pref.empty()) {
            std::size_t start = 0;
            if (buf[start] == '^') ++start;
            while ((start < buf.size()) && (!badTable[buf[start]])) {
              ++start;
            }
            struct Slice {
              std::size_t i;
              std::size_t sz;
            };
            Slice curr{0,0};
            Slice longest{0,0};
            bool ingroup = false;
            for (std::size_t i = start; i < buf.size(); ++i) {
              if (ingroup) {
                if (buf[i] == ']') {
                  ingroup = false;
                }
                curr = {.i = i+1, .sz = 0};
              } else {
                if (buf[i] == '[') {
                  ingroup = true;
                  curr = {.i = i+1, .sz = 0};
                } else {
                  if (badTable[buf[i]]) {
                    curr = {.i = i+1, .sz = 0};
                  } else if (((i+1) == buf.size()) || (!qualTable[buf[i+1]])) {
                    curr.sz++;
                  } else {
                    curr = {.i = i+1, .sz = 0};
                  }
                  if (curr.sz > longest.sz) {
                    longest = curr;
                  }
                }
              }
            }
            if (longest.i && longest.sz) {
              bool matches_any = false;
              auto& test = cache2;
              test.resize(longest.sz);
              std::copy_n(std::begin(buf)+longest.i,longest.sz,std::begin(test));
              std::size_t n = 0;
              while (!test.empty()) {
                matches_any = false;
                for (auto const& p: query.positiveL) {
                  if (matches_any |= p.contains(test)) {
                    break;
                  }
                }
                if (matches_any) {
                  break;
                } else {
                  n = longest.i + test.size();
                  test.pop_back();
                }
              }
              if (n) {
                pref.resize(n);
                std::copy_n(std::begin(buf), n, std::begin(pref));
                //std::cerr << "'" << buf << "' to prefix '" << pref << "'. buf: " << buf.size() << ", n: " << n << ", pref: " << pref.size() << ", longest.i: " << longest.i << ", longest.sz: " << longest.sz << "\n";
              }
            }
          }
          return pref;
        }
      };
      FastSkipCB fastSkipCB{};
      std::size_t fastSkip(Query const& query) {
        std::string& pref = fastSkipCB.badPrefix(query, buf);
        std::size_t const n = pref.size();
        if (n == 0)
          return 0;
        advance(pref);
        if (pref.size() > n)
          buf.push_back(chain[0]);
        while (pref.size() < buf.size()) {
          pref.push_back(chain[0]);
        }
        buf = pref;
        return 1+fastSkip(query);
      }

      bool operator==(Iterator const&) const {
        return false;
      }

      std::string const& operator*() const {
        return buf;
      }
      std::string const* operator->() const {
        return &buf;
      }
    };
    Iterator begin() const {
      return {start, chain};
    }
    Iterator end() const {
      return {std::string{}, chain};
    }
};

inline std::string renderN(std::size_t const n) {
  auto repr = std::to_string(n);
  static constexpr auto const pref = " kMGTPEZYRQ";
  std::size_t s = 0;
  while (repr.size() > 3) {
    repr.resize(repr.size()-3);
    ++s;
  }
  if (s)
    repr += pref[s];
  return repr;
}
inline std::string renderT(std::uint64_t n) {
  if (n < 5*60)
    return std::to_string(n) + "s";
  n = (n+30)/60;
  if (n < 5*60)
    return std::to_string(n) + "m";
  n = (n+30)/60;
  return std::to_string(n) + "h";
}

struct BatchResult {
  std::string solution{};
  std::uint64_t checked{0};
};

std::string dispatch(std::size_t const n, Query const& query, SortedGenerator const& gen) {
  Checker check;
  if (n == 1) {
    for (auto const& r: gen) {
      if (check(r, query)) {
        return r;
      }
    }
    throw std::runtime_error("!Unkonwn termination error.");
  } else {
    static constexpr std::size_t ledger = 64*1024;
    std::vector<std::pair<std::vector<std::string>,BatchResult>> jobs(n, std::pair{std::vector<std::string>(ledger),BatchResult{}});
    std::vector<std::pair<std::vector<std::string>,BatchResult>> jobsNext(n, std::pair{std::vector<std::string>(ledger),BatchResult{}});
    auto it = gen.begin();
    {
      auto const next = *it;
      if (auto const x = it.fastSkip(query)) {
        //std::cerr << "Skipped from '" << next << "' to '" << *it << "' with " << x << " skips\n" << std::flush;
      }
    }
    std::vector<std::thread> workers;
    workers.reserve(n);
    std::vector<std::string> solutions;
    std::size_t round = 0;
    std::uint64_t checked = 0;

    auto const prepareJobs = [&it,&query](std::vector<std::pair<std::vector<std::string>,BatchResult>>& nextJobs) {
      for (auto& p: nextJobs) {
        for (auto& s: p.first) {
          s = *it;
          ++it;
          [[maybe_unused]] auto const next = *it;
          if ([[maybe_unused]] auto const x = it.fastSkip(query)) {
            //std::cerr << "Skipped from '" << next << "' to '" << *it << "' with " << x << " skips\n" << std::flush;
          }
        }
        p.second = BatchResult{};
      }
    };

    auto const t0 = std::chrono::steady_clock::now();

    while (solutions.empty()) {
      std::uint64_t checkedRound = 0;
      auto const start = std::chrono::steady_clock::now();
      if (round == 0) {
        prepareJobs(jobs);
      } else {
        jobs.swap(jobsNext);
      }
      for (std::size_t i = 0; i < n; ++i) {
        workers.emplace_back([&](std::size_t const k) {
          auto& job = jobs[k];
          auto& result = job.second;
          for (auto& task: job.first) {
            result.checked++;
            if (check(task, query)) {
              result.solution = task;
              break;
            }
          }
        }, i);
      }
      prepareJobs(jobsNext);
      for (auto& t: workers) {
        t.join();
      }
      workers.clear();
      for (auto& p: jobs) {
        auto const& r = p.second;
        checkedRound += r.checked;
        checked += r.checked;
        if (!r.solution.empty())
          solutions.push_back(r.solution);
      }
      ++round;
      {
        auto const stop = std::chrono::steady_clock::now();
        std::chrono::duration<double> const diffL = stop-start;
        std::chrono::duration<double> const diffT = stop-t0;
        auto const tpL = static_cast<std::uint64_t>(checkedRound / diffL.count());
        auto const tpT = static_cast<std::uint64_t>(checked / diffT.count());
        std::uint64_t const totalNow = gen.ordinal(jobs.back().first.back());
        std::uint64_t const totalStart = gen.ordinal(gen.startRegex());
        std::uint64_t const total = totalNow - totalStart;
        std::cerr << "[" << round << "] checked: " << renderN(checked) << " (~" << renderN(tpL) << "/s, " << tpT << "/s). Skipped: " << renderN(total - checked) << "\t'" << jobs.front().first.front() << "' ... '" << jobs.back().first.back() << "'\n" << std::flush;
      }
    }
    for (std::size_t i = 1; i < solutions.size(); ++i) {
      if (solutions[i].size() < solutions[0].size())
        solutions[0] = solutions[i];
    }
    return solutions[0];
  }
}

inline std::vector<std::string> parseList(char const* const in) {
  std::vector<std::string> result(1);
  for (char const* p = in; *p; ++p) {
    if ((('a' <= *p) & (*p <= 'z')) | (('A' <= *p) & (*p <= 'Z')) | (*p == ' ')) {
      if (!result.back().empty() | (*p != ' ')) {
        result.back().push_back(*p);
      }
    } else if (*p == ',') {
      while (!result.back().empty() && (result.back().back() == ' ')) result.back().pop_back();
      result.emplace_back();
    } else {
      throw std::invalid_argument("Bad character '" + std::string{*p} + "' in input sequence discovered.");
    }
  }
  return result;
}

int main(int argc, char** argv) {
  if (argc < 3) {
    std::cerr << "Invoke as:\n"
    "\t" << argv[0] << " 'A New Hope, The Empire Strikes Back, Return of the Jedi' 'The Motion Picture,The Wrath of Khan, The Search for Spock, The Voyage Home, The Final Frontier, The Undiscovered Country' [optional-args]\n"
    "\tOptional arguments:\n"
    "\t\t-jN: N threads to use (default:16)\n"
    "\t\t-cregex: Continue at 'regex'. By default search starts with the first regex.\n"
    "\t\t-r: reverse positive and negative list\n";
    return 1;
  }
  std::uint16_t jobs = 16;
  std::size_t i0 = 1;
  std::size_t i1 = 2;
  std::string regex;
  for (int k = 3; k < argc; ++k) {
    std::string_view s = argv[k];
    bool processed = false;
    if (s.starts_with("-j")) {
      jobs = static_cast<std::uint16_t>(std::strtoul(s.begin()+2, nullptr, 10));
      if ("-j" + std::to_string(jobs) != s) {
        throw std::invalid_argument("Optional argument -j requires a number.");
      }
      processed = true;
    } else if (s == "-r") {
      std::swap(i0,i1);
      processed = true;
    } else if (s.starts_with("-c")) {
      regex = std::string(s.begin()+2,s.end());
      if (regex.empty())
        throw std::invalid_argument("Cannot continue from empty regex.");
      processed = true;
    }
    if (!processed) {
      throw std::invalid_argument("Unknown optional argument: '" + std::string{s} + "'");
    }
  }
  //Query query({"A New Hope", "The Empire Strikes Back", "Return of the Jedi", "The Phantom Menace", "Attack of the Clones", "Revenge of the Sith"},
  //            {"The Motion Picture", "The Wrath of Khan", "The Search for Spock", "The Voyage Home", "The Final Frontier", "The Undiscovered Country", "Generations", "First Contact", "Insurrection", "Nemesis", "Into Darkness"});
  Query query(parseList(argv[i0]), parseList(argv[i1]));
  SortedGenerator gen(query.alphabet, regex);
  auto const start = std::chrono::steady_clock::now();
  std::string const match = dispatch(jobs, query, gen);
  auto const stop = std::chrono::steady_clock::now();
  std::cout << "Finished in " << renderT(static_cast<unsigned>(std::chrono::duration<double>(stop-start).count())) << "\n";
  std::cout << "Shortest match: '" << match << "'\n";
  for (auto const& p: query.positive) {
    std::cout << "\t+" << p << '\n';
  }
  for (auto const& n: query.negative) {
    std::cout << "\t-" << n << '\n';
  }
}
